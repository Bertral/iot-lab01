
import sqlite3

# initialize database if not ready
conn = sqlite3.connect('support.db')

c = conn.cursor()

# create table users : in the real world, we would encrypt the password
c.execute(
    '''
    CREATE TABLE IF NOT EXISTS users(
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "userid" TEXT NOT NULL UNIQUE,
    "userpwd" TEXT,
    "devgroup" INTEGER)
    ''')

# create table devices
c.execute(
    '''
    CREATE TABLE IF NOT EXISTS devices(
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "roomid" INTEGER,
    "major" INTEGER NOT NULL,
    "minor" INTEGER NOT NULL,
    "valve" TEXT,
    "store" TEXT,
    "dimmer" INTEGER,
    "usrgroup" INTEGER)
    ''')

conn.commit()

# check if tables are empty
c.execute(
    '''
    SELECT count(*) FROM users
    ''')

if c.fetchall()[0][0] == 0:
    # populate users
    print('users table is empty, creating default users "user1" (access to both devices) and "user2" (access to none)')
    c.execute('''
        INSERT INTO users(userid, userpwd, devgroup) VALUES
        ('user1', 'user1', 1),
        ('user2', 'user2', 2)
        ''')
    conn.commit()

c.execute(
    '''
    SELECT count(*) FROM devices
    ''')


if c.fetchall()[0][0] == 0:
    # populate devices
    print('devices table is empty, creating default devices')

    c.execute('''
        INSERT INTO devices(roomid, major, minor, valve, store, dimmer, usrgroup)
        VALUES (6 , 36662, 17744, '0/4/1', '3/4/1', 5, 1),
        (16, 36662, 54096, '0/4/2', '3/4/2', 6, 1)
        ''')

    conn.commit()

conn.close()
