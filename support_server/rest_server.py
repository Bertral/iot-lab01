
import json
import sqlite3
from flask import Flask, render_template, jsonify, Response, request

PORT = 5001

###################
### REST SERVER ###
###################

app = Flask(__name__)

"""
@api {post} /get_devices
@apiName get_devices

@apiParam {username} user name
@apiParam {password} user password

@apiParamExample {json} example :
    {
        "username" : "user1",
        "password" : "user1"
    }
    
@apiSuccess {list} list of usable devices for this user
@apiSuccess {String} encrypted and signed list of usable devices (needed by other REST servers, unimplemented)

@apiSuccessExample {json} Example of result in case of success:
{
  "devices": [
    {
      "roomid" : 6,
      "major" : 36662,
      "minor" : 17744,
      "valve" : "0/4/1",
      "store" : "3/4/1",
      "dimmer" : 5,
    },
    {
      "roomid" : 16,
      "major" : 36662,
      "minor" : 54096,
      "valve" : "0/4/2",
      "store" : "3/4/2",
      "dimmer" : 15,
    }
  ]
  "token": "wofid32e0293uf09ufuh8ezf8wzof329hf3feuwz329f98217e73dezwudkwaheb918z2e18z2387fg8fwa"
}

@apiDescription Gets a list of all devices that the user has access to

@apiErrorExample {json} Error in case node is not a dimmer:
     {
       "error": "invalid username or password"
     }

"""
@app.route('/get_devices', methods=['POST'], strict_slashes=False)
def get_devices():
    if request.method=='POST':
        content = request.get_json()
        if all(item in content.keys() for item in ['username','password']):
            username = content['username']
            password = content['password']
            
            conn = sqlite3.connect('support.db')
            c = conn.cursor()
            
            c.execute("SELECT * FROM users WHERE userid=? AND userpwd=?", [username, password])
            user = c.fetchone()
            print(user)
            
            if user:
                # get all devices in the same group as the user
                c.execute("SELECT * FROM devices WHERE usrgroup=?", str(user[3]))
                devices = c.fetchall()
                conn.close()
                
                print(devices)
                
                response = {}
                response['devices'] = []
                
                for device in devices:
                    formatted_device = {}
                    formatted_device['roomid'] = device[1]
                    formatted_device['major'] = device[2]
                    formatted_device['minor'] = device[3]
                    formatted_device['valve'] = device[4]
                    formatted_device['store'] = device[5]
                    formatted_device['dimmer'] = device[6]
                    response['devices'].append(formatted_device)
                
                response['token'] = "placeholder for encrypted and signed device list"
                return json.dumps(response)
            else:
                conn.close()
                return '{"error":"invalid username or password"}'
        return 'wrong input'
    return 'use POST method'
    

app.run(host='::', port=PORT, debug=False, use_reloader=False)
