# IOT lab : Smart-building
This project contains all the components of the IOT lab Smart-building.
The goal is to control blinds, lights, and radiators from a smartphone.
Beacons are used to locate the user and select the devices of the room he's in.
A support layer contains the database of every available device and users.

![architecture](https://i.imgur.com/l0fCBf8.png)
(Image also available locally in the /images folder)

## KNX server
The KNX REST server is in the "knx" folder. It controls blinds and radiators.
It requires python 3, `flask` and `knxnet`. Launch it using `python knx_server.py`. It runs on port 5000.

This is what was done in part 1 of lab 1.

## Z-Wave server
The Z-Wave REST server is in the "Smart-building" folder. It controls sensors (unused by the mobile app) and lights.
It requires python 2, `openzwave`, `louie` and `flask`. Launch it using `python flask-main.py`. It runs on port 5000 by default.

This is what was done in part 2 of lab 1.

## Support Layer
The support layer is in the "support_server" folder. It contains the database of devices and users (for authentification and permissions).
It requires python 3 and `flask`. Initialize a demo database by executing `python init_demo_db.py` and run the server using `rest_server.py`. It runs on port 5001.

This server uses an SQL database to hold the information, it also uses a very simple authentification with user and password. The password is not hashed so this should be changed for a real implementation, also the connection should be over a secure HTTPS conncection with a valid certificate and not just HTTP. This was not implemented for this demo, so a simple user, password json packet was used for now.

## Mobile application
The mobile app is in the "BeaconsApp" folder. It locates the user using beacons and interacts with the physical world through the REST servers.

### Configuration
At this moment the configuration is hardcoded into the code, in a real application this should be set in the configuration menu. The main configuration are the servers to which the application connects.

On launch the application will get the environment configuration from the support layer server. The support layer gives information about the devices that can be in the rooms. For each device it gives the room id, information about the corresponding beacons in the room, valve, dimmer (light), and shutters (stores). On start the application will get all this information and make a list of Rooms with their devices. This is done by the getRoomsFromDatabase() call. This is called by pushing the "Get Info from Support Layer" button.

The support layer allows the application to link devices with the room, therefore when the buttons are pressed the commands will go to the correct rooms.

### Rooms
Each room has a distance value, an associated name, and an associated color. These are associated via maps (hashmaps). The closest distance indicates the room we are in, using the formula given. The name of the room will display on the application and the color will set the button colors. This is to have a visual aid that links the room to the color of the beacon, however this color can be set to anything you want. E.g., if you have a green room with a hidden red beacon you can set the room color as green if you like.

![Blue Room Image](images/blue_room.jpeg)

![Pink Room Image](images/pink_room.jpeg)

### Technical aspects
The onClickListeners associated with each button will perform the action by creating the json request with the correct values and sending the request to the corresponding server. If we are not in a room, the application will see this by performing a simple check (checking if the current room is in the list of known rooms) and will not perform any action if not in a room. See the onClickListeners to see the json info sent, this info is build from the info from the support layer server and the room associations. HTTP requests were implemented using an AsyncTask that is run on another thread in order not to block the application.

### Note :
Be sure to be on the correct networks to be able to access every server. E.g., My phone was connected to the WiFi access point of the raspberry Pi to connect to the servers running on the Pi (Z-Wave server) and we ran the support layer on the rapsberry Pi also, we can also run this on our PC but we must connect the Raspberry pi to the computer by cable and share the connection in order to access every server. Since our machines are linux based we configured our firewall (iptables) to transfer the packets from the PC WiFi (internet) to the rapsberry and we ran our servers on the raspberry. If we wanted to run them on the computer we could also change the iptables config of the raspberry pi to act as a gateway between the cellphone and the pc. 

## Security (unimplemented)
For real usability, the system needs to be secured. Every request coming from the mobile client can't be trusted.
There are multiple ways to fix this. For example, we could send signed and encrypted responses from the support layer.
This response could then be relayed by the mobile application to other REST servers, which can then decrypt it and verify its origin using their private keys.
Those encrypted messages would need to contain the list of available devices as well as the user trying to access them.
This way, the KNX and Z-Wave REST servers would know which devices the user is allows to use.
Another way would be to use secure connections over TLS with sessions, using session ID and tokens [wikipedia link](https://en.wikipedia.org/wiki/Token_Binding) [wikipedia link](https://en.wikipedia.org/wiki/Transport_Layer_Security)

## Authors
Friant Antoine, Wertenbroek Rick
