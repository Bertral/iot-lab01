# -*- coding: utf-8 -*-

################################################################################
#
# MSE - IOT
#
# Authors : Antoine Friant, Rick Wertenbroek
# Date    : 04.10.18
#
# Usage :
#
# Configuration IP/port :
# Remplir l'ip dans la section "Globals" ci-dessous dans les variables
# gateway_ip et gateway_port
#
# Pour l'écriture, le programme s'appelle de la façon suivante :
# knxip.py x/y/z val
#
# Pour la lecture :
# knxip.py x/y/z
################################################################################

import socket,sys
from knxnet import *

class gateway_connection(object):

    connection_status = 'Zero is OK'
    channel_id = ''
    
    def __init__(self, gateway_ip, gateway_port):
        self.gateway_ip = gateway_ip
        self.gateway_port = gateway_port
        
        # -> in this example, for sake of simplicity, the two ports are the same
        # With the simulator, the gateway_ip must be set to 127.0.0.1 and gateway_port to 3671
        our_port         = 3672
        self.data_endpoint    = ('0.0.0.0', our_port)
        self.control_endpoint = ('0.0.0.0', our_port)

        # -> Socket creation
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind(('', our_port))

    def connect_to_gateway(self):
        # TODO : Add a check to see if we are already connected

        # -> Sending Connection request
        conn_req_object = knxnet.create_frame(knxnet.ServiceTypeDescriptor.CONNECTION_REQUEST, self.control_endpoint, self.data_endpoint)
        conn_req_dtgrm = conn_req_object.frame # Serializing
        self.sock.sendto(conn_req_dtgrm, (self.gateway_ip, self.gateway_port))

        # <- Receiving Connection response
        data_recv, addr = self.sock.recvfrom(1024)
        conn_resp_object = knxnet.decode_frame(data_recv)

        # <- Retrieving channel_id from Connection response
        conn_channel_id = conn_resp_object.channel_id

        print("Channel ID is {0}".format(conn_channel_id))
        self.channel_id = conn_channel_id

        # -> Sending Connection State Request
        conn_state_req_object = knxnet.create_frame(knxnet.ServiceTypeDescriptor.CONNECTION_STATE_REQUEST, conn_channel_id, self.control_endpoint)
        conn_state_req_dtgrm = conn_state_req_object.frame # Serializing
        self.sock.sendto(conn_state_req_dtgrm, (self.gateway_ip, self.gateway_port))

        # <- Receiving Connection State Response
        data_recv, addr = self.sock.recvfrom(1024)
        conn_state_resp_object = knxnet.decode_frame(data_recv)

        print("Connexion state is :\n", conn_state_resp_object)
        self.connection_status = conn_state_resp_object.status
        
    def disconnect_from_gateway(self):
        disconnect_request_object = knxnet.create_frame(knxnet.ServiceTypeDescriptor.DISCONNECT_REQUEST, self.channel_id, self.control_endpoint)
        disconnect_request_dtgrm = disconnect_request_object.frame
        self.sock.sendto(disconnect_request_dtgrm, (self.gateway_ip, self.gateway_port))
        
        data_recv, addr = self.sock.recvfrom(1024)
        disconnect_response_object = knxnet.decode_frame(data_recv)
        
        #print("Disconnect response :\n", disconnect_response_object)
        self.connection_status = disconnect_response_object.status

    def write_to_object(self, dest_addr_group, data):
        
        self.connect_to_gateway()
        
        data_size = 2
        apci = 0x2 # Write query

        tun_req_object = knxnet.create_frame(knxnet.ServiceTypeDescriptor.TUNNELLING_REQUEST,
                                             dest_addr_group,
                                             self.channel_id,
                                             data,
                                             data_size,
                                             apci)
        tun_req_dtgrm = tun_req_object = tun_req_object.frame
        self.sock.sendto(tun_req_dtgrm, (self.gateway_ip, self.gateway_port))

        # <- Receiving Tunneling Request Response (ACK)
        data_recv, addr = self.sock.recvfrom(1024)
        tun_state_resp_object = knxnet.decode_frame(data_recv)

        # Upon receipt of the "Tunneling Request", the KNXnet/IP gateway returns an acknowledgment
        # ("Tunneling Ack") with an error code (called "status") "00" if there has been no error.
        print("Tunnelling response is :\n", tun_state_resp_object)

        # <- Receiving a Tunneling Request from gateway
        data_recv, addr = self.sock.recvfrom(1024)
        tun_req_recv_object = knxnet.decode_frame(data_recv)
        # The KNXnet/IP interface checks our "Tunneling Request" and returns it with the "Data
        # Service" field set to 0x2e ("Data.confirmation").
        print(tun_req_recv_object)
        if tun_req_recv_object.data_service == 0x2e:
            print("Data service field is OK")
        else:
            print("Data service field is wrong")

        # The client program compares the Tunneling Request already sent and the Tunneling Request
        # returned by the KNXnet/IP gateway. If there are no transmission errors, the client program
        # sends an acknowledgment ("Tunneling Ack") with an error code (called "status") "00" (zero).

        # -> Sending ACK to gateway's tunnelling req
        status = 0 # ok
        sequence_counter = tun_req_recv_object.sequence_counter
        tun_ack_object = knxnet.create_frame(knxnet.ServiceTypeDescriptor.TUNNELLING_ACK,
                                             self.channel_id,
                                             status,
                                             sequence_counter)
        self.sock.sendto(tun_ack_object.frame, (self.gateway_ip, self.gateway_port))
        
        self.disconnect_from_gateway()

    def read_from_object(self, dest_addr_group):
        
        self.connect_to_gateway()
        
        data = 0
        data_size = 2
        apci = 0x0 # Read query

        # Will need apci of 1 to respond to query

        tun_req_object = knxnet.create_frame(knxnet.ServiceTypeDescriptor.TUNNELLING_REQUEST,
                                             dest_addr_group,
                                             self.channel_id,
                                             data,
                                             data_size,
                                             apci)
        tun_req_dtgrm = tun_req_object = tun_req_object.frame
        self.sock.sendto(tun_req_dtgrm, (self.gateway_ip, self.gateway_port))

        # <- Receiving Tunneling Request Response (ACK)
        data_recv, addr = self.sock.recvfrom(1024)
        tun_state_resp_object = knxnet.decode_frame(data_recv)

        # Upon receipt of the "Tunneling Request", the KNXnet/IP gateway returns an acknowledgment
        # ("Tunneling Ack") with an error code (called "status") "00" if there has been no error.
        print("Tunnelling response is :\n", tun_state_resp_object)

        # <- Receiving a Tunneling Request from gateway
        data_recv, addr = self.sock.recvfrom(1024)
        tun_req_recv_object = knxnet.decode_frame(data_recv)
        # The KNXnet/IP interface checks our "Tunneling Request" and returns it with the "Data
        # Service" field set to 0x2e ("Data.confirmation").
        print(tun_req_recv_object)
        if tun_req_recv_object.data_service == 0x2e:
            print("Data service field is OK")
        else:
            print("Data service field is wrong")

        # The client program compares the Tunneling Request already sent and the Tunneling Request
        # returned by the KNXnet/IP gateway. If there are no transmission errors, the client program
        # sends an acknowledgment ("Tunneling Ack") with an error code (called "status") "00" (zero).

        # -> Sending ACK to gateway's tunnelling req
        status = 0 # ok
        sequence_counter = tun_req_recv_object.sequence_counter
        tun_ack_object = knxnet.create_frame(knxnet.ServiceTypeDescriptor.TUNNELLING_ACK,
                                             self.channel_id,
                                             status,
                                             sequence_counter)
        self.sock.sendto(tun_ack_object.frame, (self.gateway_ip, self.gateway_port))

        # The read tunneling req comes here

        # <- Receiving a Tunneling Request from gateway
        data_recv, addr = self.sock.recvfrom(1024)
        tun_req_recv_object = knxnet.decode_frame(data_recv)
        # The KNXnet/IP interface checks our "Tunneling Request" and returns it with the "Data

        print("DATA : {0}\n".format(tun_req_recv_object.data))
        
        self.disconnect_from_gateway()

        return tun_req_recv_object.data

