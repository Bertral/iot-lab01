import socket,sys
from knxnet import *

# "Globals"
gateway_ip   = "localhost"
gateway_port = 3671 


ctg = gateway_connection(gateway_ip, gateway_port)

if len(sys.argv) < 2 :
    print("Usage :\nknxip.py x/y/z val\nknxip.py x/y/z")
elif len(sys.argv) < 3 :
    ctg.read_from_object(knxnet.GroupAddress.from_str(sys.argv[1]))
else:
    ctg.write_to_object(knxnet.GroupAddress.from_str(sys.argv[1]), int(sys.argv[2]))
