import requests
import json
import sys

#############################################################
#### This script sends POST or PUT http request to server ###
#############################################################



if len(sys.argv) == 1:
    print("Usage examples :")
    print("pyhton2 post_client.py get_nodes_list")
    print("pyhton2 post_client.py get_all_measures 5")
    print("pyhton2 post_client.py 120 5")
    print("pyhton2 post_client.py inclusion")
    print("pyhton2 post_client.py exclusion")
else:
    #### Configuration of nodes
    #req = requests.post('http://192.168.1.2:5000/network/set_sensor_nodes_basic_configuration',headers={'Content-Type': 'application/json'}, data=json.dumps({'Group_Interval': '240','Group_Reports':'240', 'Wake-up_Interval': '480'}))

    #print (req.text)  # print server response

    if sys.argv[1] == 'get_nodes_list':
        #### Send command to switch
        req = requests.get('http://192.168.1.2:5000/nodes/get_nodes_list')
    elif sys.argv[1] == 'get_all_measures':
        #### Send command to switch
        req = requests.get('http://192.168.1.2:5000/sensors/{0}/get_all_measures'.format(sys.argv[2]))
    elif sys.argv[1] == 'inclusion':
        #### Put controller in inclusion mode
        req = requests.post('http://192.168.1.2:5000/nodes/add_node')
    elif sys.argv[1] == 'exclusion':
        #### Put controller in exclusion mode
        req = requests.post('http://192.168.1.2:5000/nodes/remove_node')
    else:
        req = requests.post('http://192.168.1.2:5000/dimmers/set_level',headers={'Content-Type': 'application/json'}, data=json.dumps({'node_id': int(sys.argv[2]),'value': int(sys.argv[1])}))
    
    print (req.text)  # print server response


#### Config a specific parameter
#req = requests.post('http://192.168.1.2:5000/nodes/set_parameter',headers={'Content-Type': 'application/json'}, data=json.dumps({'node_id': '4','value':'480', 'parameter_index': '111', 'size': '4'}))


#### Set node location
#req = requests.post('http://192.168.1.2:5000/nodes/set_location',headers={'Content-Type': 'application/json'}, data=json.dumps({'node_id': '4','value':'A402'}))


#### Set node name
#req = requests.post('http://192.168.1.2:5000/nodes/set_name',headers={'Content-Type': 'application/json'}, data=json.dumps({'node_id': '4','value':'sensor'}))


#### Send command to switch
#req = requests.post('http://192.168.1.2:5000/switches/send_command',headers={'Content-Type': 'application/json'}, data=json.dumps({'node_id': '3','value':'on'}))

#### Send command to dimmer
#req = requests.post('http://192.168.1.2:5000/dimmers/set_level',headers={'Content-Type': 'application/json'}, data=json.dumps({'node_id': '6','value':'120'}))

#### Put controller in inclusion mode
#req = requests.post('http://192.168.1.2:5000/nodes/add_node')


#### Put controller in exclusion mode
#req = requests.post('http://192.168.1.2:5000/nodes/remove_node')


#print (req.text)  # print server response
