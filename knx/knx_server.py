# use python3
import sys
import os

from knxnet import *
from flask import Flask, render_template, jsonify, Response, request
from knxip import gateway_connection

REST_PORT = 5000

gateway_ip   = "localhost"
gateway_port = 3671

ctg = gateway_connection(gateway_ip, gateway_port)
app = Flask(__name__)

"""
@api {get/post} /<x>/<y>/<z>
@apiName get_value

@apiParam {Number} value new value for device x/y/z for POST, nothing for GET

@apiParamExample {json} POST example :
    {
        'value' : '100',
    }

@apiSuccess {String} 'success' for POST, number for GET

@apiDescription GET the value for device x/y/z or POST a new valus for device x/y/z
"""
@app.route('/<int:x>/<int:y>/<int:z>', methods=['GET', 'POST'], strict_slashes=False)
def add_node(x, y, z):
    content = request.get_json()
    
    if request.method == 'GET':
        return str(ctg.read_from_object(knxnet.GroupAddress.from_str('{0}/{1}/{2}'.format(x, y, z))))
    
    if request.method == 'POST':
        ctg.write_to_object(knxnet.GroupAddress.from_str('{0}/{1}/{2}'.format(x, y, z)), int(content['value']))
        return 'success'

try:
    app.run(host='::', port=REST_PORT, debug=False, use_reloader=False)

except KeyboardInterrupt:
    backend.stop()
