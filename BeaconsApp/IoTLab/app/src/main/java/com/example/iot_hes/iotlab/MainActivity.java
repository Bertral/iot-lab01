/** Authors : Antoine Friant & Rick Wertenbroek
 *  IoT Lab 1 - Part 3
 *  Date : 26.10.18
 */

package com.example.iot_hes.iotlab;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import com.estimote.coresdk.observation.region.beacon.BeaconRegion;
import com.estimote.coresdk.recognition.packets.Beacon;
import com.estimote.coresdk.service.BeaconManager;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    TextView PositionText;
    EditText Percentage;
    Button   IncrButton;
    Button   DecrButton;
    Button   LightButton;
    Button   StoreButton;
    Button   RadiatorButton;
    Button   GetInfoButton;

    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    private final String username = "user1";
    private final String password = "user1"; /* Pas top d'avoir ceci en clair ici, mais c'est juste pour la demo */
    private String token;

    private final String myuuid = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
    private final int mymajor = 36662;
    private final int myminor1 = 17744; // TODO : Make list
    private final int myminor2 = 54096;
    private String room = "No room";
    private int current_room_minor = 0;
    private List<Integer> myMinorList = new ArrayList<>();
    private List<Room> myRoomList = new ArrayList<>();
    private Map<Integer, Room> myRoomMap = new HashMap<>();
    private Map<Integer, Double> distanceMap = new HashMap<>();
    private Map<Integer, String> nameMap = new HashMap<>();
    private Map<Integer, Integer> colorMap = new HashMap<>();
    private List<Integer> coloredButtonList = new ArrayList<>();
    private double current_closest_distance = 0.0;


    private BeaconManager beaconManager;
    private BeaconRegion monitoringRegionBlue = new BeaconRegion("Blue Room", UUID.fromString(myuuid), mymajor, myminor1);
    private BeaconRegion monitoringRegionPink  = new BeaconRegion("Pink Room", UUID.fromString(myuuid), mymajor, myminor2);

    // In the "OnCreate" function below:
    // - TextView, EditText and Button elements are linked to their graphical parts (Done for you ;) )
    // - "OnClick" functions for Increment and Decrement Buttons are implemented (Done for you ;) )
    //
    // TODO List:
    // - Use the Estimote SDK to detect the closest Beacon and figure out the current Room
    //     --> See Estimote documentation:  https://github.com/Estimote/Android-SDK
    // - Set the PositionText with the Room name
    // - Implement the "OnClick" functions for LightButton, StoreButton and RadiatorButton


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android M Permission check
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("This app needs location access");
                builder.setMessage("Please grant location access so this app can detect beacons.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                    }
                });
                builder.show();
            }
        }

        PositionText   =  findViewById(R.id.PositionText);
        Percentage     =  findViewById(R.id.Percentage);
        IncrButton     =  findViewById(R.id.IncrButton);
        DecrButton     =  findViewById(R.id.DecrButton);
        LightButton    =  findViewById(R.id.LightButton);
        StoreButton    =  findViewById(R.id.StoreButton);
        RadiatorButton =  findViewById(R.id.RadiatorButton);
        GetInfoButton  =  findViewById(R.id.GetInfoButton);
        coloredButtonList.add(R.id.RadiatorButton);
        coloredButtonList.add(R.id.StoreButton);
        coloredButtonList.add(R.id.LightButton);
        coloredButtonList.add(R.id.IncrButton);
        coloredButtonList.add(R.id.DecrButton);

        myMinorList.add(myminor1);
        myMinorList.add(myminor2);
        nameMap.put(myminor1, "Blueberry Room");
        nameMap.put(myminor2, "Candy Room");
        colorMap.put(myminor1, R.color.blue);
        colorMap.put(myminor2, R.color.pink);

        beaconManager = new BeaconManager(getApplicationContext());
        beaconManager.setMonitoringListener(new BeaconManager.BeaconMonitoringListener() {
            @Override
            public void onEnteredRegion(BeaconRegion beaconRegion, List<Beacon> list) {
                beaconManager.startRanging(beaconRegion);
                Toast.makeText(getApplicationContext(), "Entered region " + beaconRegion.getIdentifier(), Toast.LENGTH_LONG).show();
                //room = beaconRegion.getIdentifier();
                //TextView tv = findViewById(R.id.PositionText);
                //tv.setText(room);
            }

            @Override
            public void onExitedRegion(BeaconRegion beaconRegion) {
                beaconManager.stopRanging(beaconRegion);
                Toast.makeText(getApplicationContext(), "Exited region " + beaconRegion.getIdentifier(), Toast.LENGTH_LONG).show();
            }
        });
        beaconManager.setRangingListener(new BeaconManager.BeaconRangingListener() {
            @Override
            public void onBeaconsDiscovered(BeaconRegion region, List<Beacon> beacons) {
                if (beacons.size() != 0) {
                    Beacon b = beacons.get(0);
                    int txPow = b.getMeasuredPower();
                    int rssi = b.getRssi();
                    double d = (double)(txPow - rssi) / (10.0*2.0);

                    distanceMap.put(b.getMinor(), d);

                    current_closest_distance = Collections.min(distanceMap.values());

                    // == compare is ok since they represent the same exact value
                    if (d == current_closest_distance) {
                        room = region.getIdentifier();
                        current_room_minor = b.getMinor();
                        TextView tv = findViewById(R.id.PositionText);
                        tv.setText(room);
                        for (Integer i : coloredButtonList) {
                            findViewById(i).setBackgroundTintList(getApplicationContext().getResources().getColorStateList(colorMap.get(b.getMinor())));
                        }
                    }
                }
            }
        });

        // If needed set the scan periods
        //beaconManager.setBackgroundScanPeriod(100, 100);
        //beaconManager.setForegroundScanPeriod(100,100);
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startMonitoring(monitoringRegionBlue);
                beaconManager.startMonitoring(monitoringRegionPink);
            }
        });

        TextView tv = findViewById(R.id.PositionText);
        tv.setText(room);

        GetInfoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getRoomsFromDataBase();
                for (Room r : myRoomList)
                    myRoomMap.put(r.minor, r);
            }
        });

        // Only accept input values between 0 and 100
        Percentage.setFilters(new InputFilter[]{new InputFilterMinMax("0", "100")});

        IncrButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int number = Integer.parseInt(Percentage.getText().toString());
                if (number<100) {
                    number++;
                    Log.d("IoTLab-Inc", String.format("%d",number));
                    Percentage.setText(String.format("%d",number));
                }
            }
        });

        DecrButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int number = Integer.parseInt(Percentage.getText().toString());
                if (number>0) {
                    number--;
                    Log.d("IoTLab-Dec", String.format("%d",number));
                    Percentage.setText(String.format("%d",number));
                }
            }
        });







        LightButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Send HTTP Request to command light
                if (current_room_minor != 0) {
                    Log.d("IoTLab", Percentage.getText().toString());
                    Integer value = Integer.valueOf(Percentage.getText().toString());
                    String jsonString = "{\"node_id\":\"" + myRoomMap.get(current_room_minor).dimmer.toString() + "\", \"value\":\"" + value.toString() + "\"}";
                    new HTTPCallClass().execute("http://192.168.2.1:5000/dimmers/set_level", jsonString);
                }
            }
        });



        StoreButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Send HTTP Request to command store
                if (current_room_minor != 0) {
                    Log.d("IoTLab", Percentage.getText().toString());
                    Integer value = Integer.valueOf(Percentage.getText().toString()) * 255 / 100;
                    String jsonString = "{\"value\":\"" + value.toString() + "\"}";
                    String url = "http://192.168.1.2:5000/" + myRoomMap.get(current_room_minor).store;
                    new HTTPCallClass().execute(url, jsonString);
                }
            }
        });



        RadiatorButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Send HTTP Request to command radiator
                if (current_room_minor != 0) {
                    Log.d("IoTLab", Percentage.getText().toString());
                    Integer value = Integer.valueOf(Percentage.getText().toString()) * 255 / 100;
                    String jsonString = "{\"value\":\"" + value.toString() + "\"}";
                    String url = "http://192.168.1.2:5000/" + myRoomMap.get(current_room_minor).valve;
                    new HTTPCallClass().execute(url, jsonString);
                }
            }
        });


    }


    // You will be using "OnResume" and "OnPause" functions to resume and pause Beacons ranging (scanning)
    // See estimote documentation:  https://developer.estimote.com/android/tutorial/part-3-ranging-beacons/
    @Override
    protected void onResume() {
        super.onResume();
        beaconManager.startMonitoring(monitoringRegionBlue);
        beaconManager.startMonitoring(monitoringRegionPink);
    }


    @Override
    protected void onPause() {
        super.onPause();
        beaconManager.stopMonitoring(monitoringRegionBlue.toString());
        beaconManager.stopMonitoring(monitoringRegionPink.toString());
    }

    private void getRoomsFromDataBase() {

        /* Connect to the support server and ask for rooms */
        String url = "http://192.168.2.1:5001/get_devices";
        String jsonString = "{\"username\":\"" + username + "\", \"password\":\"" + password + "\"}";
        OutputStream out = null;
        DataBaseInfo dbinfo = null;

        Gson gson = new Gson();
        try {
            dbinfo = gson.fromJson(new HTTPPostAsyncTask().execute(url, jsonString).get(), DataBaseInfo.class);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (dbinfo != null) {
            myRoomList.clear();
            this.token = dbinfo.token;
            for (Devices d : dbinfo.devices) {
                myRoomList.add(new Room(d.roomid.toString(), d.major, d.minor, d.dimmer, d.store, d.valve));
                }
        }
    }

    /* Helper Classes for Deserialization */
    private class Devices {
        public Integer roomid;
        public Integer major;
        public Integer minor;
        public String valve;
        public String store;
        public Integer dimmer;
    }

    private class DataBaseInfo {
        public List<Devices> devices;
        public String token;
    }
}

class Room {
    public final String name;
    public final Integer major;
    public final Integer minor;
    public final Integer dimmer;
    public final String store;
    public final String valve;

    public Room(String name, Integer major, Integer minor, Integer dimmer, String store, String valve) {
        this.name     = name;
        this.major    = major;
        this.minor    = minor;
        this.dimmer   = dimmer;
        this.store    = store;
        this.valve    = valve;
    }
}



// This class is used to filter input, you won't be using it.

class InputFilterMinMax implements InputFilter {
    private int min, max;

    public InputFilterMinMax(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public InputFilterMinMax(String min, String max) {
        this.min = Integer.parseInt(min);
        this.max = Integer.parseInt(max);
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            int input = Integer.parseInt(dest.toString() + source.toString());
            if (isInRange(min, max, input))
                return null;
        } catch (NumberFormatException nfe) { }
        return "";
    }

    private boolean isInRange(int a, int b, int c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}