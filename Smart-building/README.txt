Authors : Friant Antoine, Wertenbroek Rick

## Client usage examples ##

Get nodes list:
pyhton2 post_client.py get_nodes_list

Get measures for node 5:
pyhton2 post_client.py get_all_measures 5

Set dimmer 6 level to 100 :
python2 post_client.py 100 6

Set inclusion mode (not implemented) :
python2 post_client.py inclusion

Set exclusion mode (not implemented) :
python2 post_client.py exclusion
