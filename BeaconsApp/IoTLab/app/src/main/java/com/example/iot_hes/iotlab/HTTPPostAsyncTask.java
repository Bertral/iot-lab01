package com.example.iot_hes.iotlab;

import android.os.AsyncTask;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPPostAsyncTask extends AsyncTask<String, Void, String> {

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        String urlString = params[0]; // URL to call
        String data = params[1]; //data to post
        OutputStream out = null;

        StringBuilder responseStrBuilder = null;

        try {
            URL url = new URL("http://192.168.2.1:5001/get_devices");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            out = new BufferedOutputStream(urlConnection.getOutputStream());

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
            writer.write(data);
            writer.flush();
            writer.close();
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            urlConnection.connect();
            urlConnection.getResponseCode();

            responseStrBuilder = new StringBuilder();
            String inputStr;
            while ((inputStr = in.readLine()) != null)
                responseStrBuilder.append(inputStr);

            urlConnection.disconnect();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return responseStrBuilder.toString();
    }

}
